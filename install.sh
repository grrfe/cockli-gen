#!/usr/bin/env bash
tag=$(git describe --tags)
/usr/bin/python -E -m pip install "cockli-gen @ git+https://gitlab.com/grrfe/cockli-gen@${tag}"
