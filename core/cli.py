#!/usr/bin/python3.10
import requests
import random
import secrets
import argparse
import string
from tempfile import NamedTemporaryFile
from bs4 import BeautifulSoup
import webbrowser

__COCK_LI_MAIL_SERVER = "mail.cock.li"


def gen_string(length, special_chars=False):
    chars = string.ascii_letters + string.digits
    if special_chars:
        chars += string.punctuation

    return "".join(secrets.choice(chars) for _ in range(length))


def generate(args):
    session = requests.Session()
    session.headers.update({"Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
                            "Accept-Encoding": "deflate",
                            "Accept-Language": "en-US,en;q=0.8",
                            "Connection": "keep-alive",
                            "User-Agent": "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:64.0) Gecko/20100101 Firefox/64.0"})

    register_request = session.get("https://cock.li/register")

    soup = BeautifulSoup(register_request.content, "html.parser")

    form = soup.body.form
    token = form.input["value"]

    captcha = form.findAll("div", {"class": "col-xs-4 text-right"})
    with NamedTemporaryFile("w", delete=False, suffix=".html") as file:
        file.write("<html><body><center>{}</center></body></html>".format(str(captcha[0])))

    webbrowser.open("file://{}".format(file.name))

    captcha_input = input("Input the captcha: ")

    username = gen_string(random.randint(args.minimum_username_length, args.maximum_username_length))
    password = gen_string(args.password_length, True)

    register_post = session.post("https://cock.li/register", data={"_token": str(token),
                                                                   "username": username,
                                                                   "domain": args.domain,
                                                                   "password": password,
                                                                   "password_confirmation": password,
                                                                   "captcha": captcha_input.rstrip(),
                                                                   "agree": "on"})

    post_parser = BeautifulSoup(register_post.content, "html.parser")
    alerts = post_parser.findAll("div", {"class": "alert"})

    if len(alerts) == 0:
        if args.simple:
            print("{0}@{1}:{2}".format(username, args.domain, password))
        else:
            print("Account created: ")
            print("Username: {0}@{1}".format(username, args.domain))
            print("Password: {}".format(password))
    else:
        print("Failed to create account: ")
        for item in alerts[0].ul.li:
            print(item)


def parse_input():
    parser = argparse.ArgumentParser(description="Create a random cock.li mail address")
    parser.add_argument("-d", "--domain", help="Select one of cock.li's many domains; default = cock.li",
                        default="cock.li")
    parser.add_argument("-p", "--password-length", help="Override the default password length; default = 32, min = 8, "
                                                        "max = 255", default=32)
    parser.add_argument("-mil", "--minimum-username-length", help="Override the default minimum username length; "
                                                                  "default = 10, min = 1", default=10)
    parser.add_argument("-mal", "--maximum-username-length", help="Override the default maximum username length; "
                                                                  "default = 16, max = 32", default=16)
    parser.add_argument("-s", "--simple", help="Simple output, username:password")
    generate(parser.parse_args())


if __name__ == "__main__":
    parse_input()
